package com.ronan.kowts

import com.ronan.kowts.actions.Constants
import com.ronan.kowts.contracts.SettingsContract
import com.ronan.kowts.presenters.SettingsPresenter
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.atLeastOnce
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner.Silent::class)
class SettingsPresenterTest {

    @Mock
    private lateinit var settingsView : SettingsContract.View

    private lateinit var settingsPresenter: SettingsPresenter

    @Before
    fun setup(){
        settingsPresenter = SettingsPresenter(settingsView)
    }

    @Test
    fun `checkDarkMode should set Night Mode when key is dark mode`(){
        settingsPresenter.checkDarkMode(Constants.SettingsStrings.DARK_MODE, true)
        verify(settingsPresenter.settingsView, atLeastOnce()).setNightMode()
    }

    @Test
    fun `checkDarkMode should set Day Mode when false`(){
        settingsPresenter.checkDarkMode("random", false)
        verify(settingsPresenter.settingsView, atLeastOnce()).setDayMode()
    }
}