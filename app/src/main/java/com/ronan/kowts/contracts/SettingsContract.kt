package com.ronan.kowts.contracts

interface SettingsContract {

    interface View {
        fun setNightMode()
        fun setDayMode()
    }

    interface Actions {
        fun checkDarkMode(key: String, isDarkMode: Boolean)
    }
}