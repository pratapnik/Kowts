package com.ronan.kowts.actions

open class Constants{
    object SettingsStrings {
        const val DARK_MODE = "darkMode"
    }
}