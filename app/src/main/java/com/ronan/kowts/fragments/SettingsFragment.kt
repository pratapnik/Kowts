package com.example.presence.fragments

import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.PreferenceFragmentCompat
import com.ronan.kowts.R
import com.ronan.kowts.actions.Constants
import com.ronan.kowts.contracts.SettingsContract
import com.ronan.kowts.presenters.SettingsPresenter

class SettingsFragment : PreferenceFragmentCompat(), SettingsContract.View {

    private lateinit var sharedPreferenceChangeListener: SharedPreferences.OnSharedPreferenceChangeListener

    var darkModeListener: DarkModeListener? = null
    lateinit var settingsPresenter: SettingsPresenter

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.root_preferences)

        settingsPresenter = SettingsPresenter(this)

        sharedPreferenceChangeListener = OnSharedPreferenceChangeListener { prefs, key ->
            settingsPresenter.checkDarkMode(
                key, prefs.getBoolean(
                    Constants.SettingsStrings.DARK_MODE,
                    false
                )
            )
        }
    }

    override fun onResume() {
        super.onResume()
        preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener(
            sharedPreferenceChangeListener
        )
    }

    override fun onPause() {
        super.onPause()
        preferenceScreen.sharedPreferences.unregisterOnSharedPreferenceChangeListener(
            sharedPreferenceChangeListener
        )
    }

    interface DarkModeListener {
        fun onDarkModeSwitch(isSwitched: Boolean)
    }

    override fun setNightMode() {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        darkModeListener?.onDarkModeSwitch(true)
    }

    override fun setDayMode() {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        darkModeListener?.onDarkModeSwitch(false)
    }

}
