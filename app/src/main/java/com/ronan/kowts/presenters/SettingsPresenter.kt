package com.ronan.kowts.presenters

import com.ronan.kowts.actions.Constants
import com.ronan.kowts.contracts.SettingsContract

class SettingsPresenter constructor(var settingsView: SettingsContract.View) :
    SettingsContract.Actions {

    override fun checkDarkMode(key: String, isDarkMode: Boolean) {
        if (key == Constants.SettingsStrings.DARK_MODE && isDarkMode)
            settingsView.setNightMode()
        else
            settingsView.setDayMode()
    }

}